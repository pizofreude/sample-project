# Hello World

I'm a sample file.

## Add alias to notepad++

By default, we can call notepad in any terminal to open notepad and notepad <fileName> will open the file in notepad as detached mode.

After installing git for windows or git for any other OS, we can call vi and nano for vim and GNU Nano text editor respectively.

However, even after installing notepad++, we cannot call notepad++ to invoke it.
There is a trick to enable this feature. Follow these steps:
1. Open any terminal.
2. Type in: alias npp='start notepad++'
3. Now if we call npp it will open notepad++ app.
4. npp <fileName> will open the file in notepad++.

That's it, keep it short and simple!
